import React, { createContext, useReducer, useCallback, useMemo } from 'react'
import { type DataStateType, type ActionMapType, type DataContextType, type Character, type ComicData } from './types'
import axios, { type InternalAxiosRequestConfig } from 'axios'

enum Types {
  SET_COMICS = 'SET_COMICS',
  SET_CHARACTERS = 'SET_CHARACTERS'
}

interface Payload {
  [Types.SET_COMICS]: {
    comics: ComicData
  }
  [Types.SET_CHARACTERS]: {
    characters: Character[]
  }
}

type ActionsType = ActionMapType<Payload>[keyof ActionMapType<Payload>]

const initialState: DataStateType = {
  comics: {
    results: []
  },
  characters: []
}

const reducer = (state: DataStateType, action: ActionsType): DataStateType => {
  switch (action.type) {
    case Types.SET_COMICS:
      return {
        ...state,
        comics: action.payload.comics
      }

    case Types.SET_CHARACTERS:
      return {
        ...state,
        characters: action.payload.characters
      }
  }
}

export const DataContext = createContext<DataContextType | null>(null)

interface DataProviderProps {
  children: React.ReactNode
}

const http = axios.create({
  baseURL: import.meta.env.VITE_MARVEL_BASE_URL
})

http.interceptors.request.use((config): InternalAxiosRequestConfig<any> | Promise<InternalAxiosRequestConfig<any>> => {
  config.params = config.params ?? {}
  config.params.apikey = import.meta.env.VITE_MARVEL_PUBLIC_KEY
  return config
})

export function DataProvider ({ children }: DataProviderProps): JSX.Element {
  const [state, dispatch] = useReducer(reducer, initialState)

  const fetchComics = useCallback(async (offset?: number) => {
    try {
      const results = await http.get('characters/1011334/comics', {
        params: {
          limit: 4,
          offset: offset ?? 0
        }
      })

      dispatch({
        type: Types.SET_COMICS,
        payload: {
          ...state,
          comics: results.data.data
        }
      })
    } catch (error) {
      // We should log this to our monitoring tool (if we had one)
      console.log(error)
    }
  }, [])

  const fetchCharacters = useCallback(async () => {
    try {
      const results = await http.get('characters', {
        params: {
          limit: 5
        }
      })

      dispatch({
        type: Types.SET_CHARACTERS,
        payload: {
          ...state,
          characters: results.data.data.results
        }
      })
    } catch (error) {
      // We should log this to our monitoring tool (if we had one)
      console.log(error)
    }
  }, [])

  const memoizedValue = useMemo(
    () => ({
      comics: state.comics,
      characters: state.characters,
      fetchComics,
      fetchCharacters
    }),
    [
      state.comics,
      state.characters,
      fetchComics,
      fetchCharacters
    ]
  )

  return (
    <DataContext.Provider value={memoizedValue}>
      {children}
    </DataContext.Provider>
  )
}
