interface Creator {
  name: string
  resourceURI: string
  role: string
}

interface ComicDate {
  date: Date
  type: string
}

export interface Comic {
  creators: {
    available: number
    collectionURI: string
    items: Creator[]
    returned: number
  }
  dates: ComicDate[]
  description: string
  thumbnail: {
    path: string
    extension: string
  }
  title: string
}

export interface Character {
  name: string
  collectionUri: string
}

export interface ComicData {
  results: Comic[]
  offset?: number
  limit?: number
  total?: number
  count?: number
}

export type ActionMapType<M extends Record<string, any>> = {
  [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key
      }
    : {
        type: Key
        payload: M[Key]
      };
}

export interface DataStateType {
  comics: ComicData
  characters: Character[]
}

export interface DataContextType {
  comics: ComicData
  fetchComics: (offset?: number) => void
  characters: Character[]
  fetchCharacters: () => void
}
