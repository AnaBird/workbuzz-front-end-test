export const mockComicData = {
  results: [{
    creators: {
      available: 3,
      collectionURI: '',
      items: [{
        name: 'Christos Gage',
        resourceURI: 'http://gateway.marvel.com/v1/public/creators/11765',
        role: 'writer'
      }, {
        name: 'Dan Slott',
        resourceURI: 'http://gateway.marvel.com/v1/public/creators/12983',
        role: 'writer'
      }, {
        name: 'Jay David Ramos',
        resourceURI: 'http://gateway.marvel.com/v1/public/creators/8706',
        role: 'colorist'
      }],
      returned: 3
    },
    dates: [{
      date: new Date('2008-09-24T00:00:00-0400'),
      type: 'onsaleDate'
    }],
    description: 'Mock description',
    title: 'Avengers: The Initiative (2007) #17',
    thumbnail: {
      extension: 'jpg',
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/a0/58dd03dc2ec00'
    }
  }],
  total: 4,
  offset: 0,
  count: 4,
  limit: 4
}
