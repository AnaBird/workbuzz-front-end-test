import { useContext } from 'react'
import { DataContext } from './DataContext'
import { type DataContextType } from './types'

export const useDataContext = (): DataContextType => {
  const context = useContext(DataContext)

  if (context == null) {
    throw new Error('useDataContext context must be used inside DataProvider')
  }

  return context
}
