import { createTheme } from '@mui/material'
import { colors } from './colors'

export const theme = createTheme({
  typography: {
    h1: {
      fontFamily: 'Roboto Condensed'
    },
    h2: {
      fontFamily: 'Roboto Condensed',
      fontSize: '31px',
      fontWeight: 700,
      color: colors.white
    },
    h4: {
      fontFamily: 'Roboto Condensed',
      fontSize: '18px',
      fontWeight: 700,
      color: colors.white,
      lineHeight: '25px'
    },
    body1: {
      fontFamily: 'Roboto',
      fontSize: '18px',
      fontWeight: 400,
      color: colors.white,
      lineHeight: '27px'
    },
    body2: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      fontWeight: 400,
      color: colors.neutral70,
      lineHeight: '21px'
    }
  },
  palette: {
    background: {
      default: colors.background
    }
  },
  components: {
    MuiContainer: {
      styleOverrides: {
        root: {
          color: '#000'
        }
      }
    }
  }
})
