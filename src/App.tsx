import React from 'react'
import { CssBaseline, ThemeProvider, Container } from '@mui/material'

import { theme } from './theme/theme'
import { DataProvider } from './context/DataContext'
import Header from './components/Header/Header'
import CharacterList from './components/CharacterList/CharactersList'

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <DataProvider>
        <Header />
        <Container>
          <CharacterList />
        </Container>
      </DataProvider>
    </ThemeProvider>
  )
}

export default App
