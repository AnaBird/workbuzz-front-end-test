import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Welcome from './Header'

it('shows the welcome text', () => {
  const { baseElement } = render(<Welcome />)
  expect(baseElement).toBeInTheDocument()
})
