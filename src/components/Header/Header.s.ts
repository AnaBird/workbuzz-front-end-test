
import { Box, styled } from '@mui/material'
import { colors } from '../../theme/colors'

export const Navigation = styled(Box)({
  backgroundColor: colors.background,
  padding: '20px 0'
})
