import React from 'react'
import { Container, Grid } from '@mui/material'
import image from '../../assets/marvel_logo.svg'

import { Navigation } from './Header.s'

const Header: React.FC = () => {
  return (
    <Navigation>
      <Container>
        <Grid container alignItems="center" justifyContent="center">
          <Grid item>
            <img src={image} alt="logo" width={150} />
          </Grid>
        </Grid>
      </Container>
    </Navigation>
  )
}

export default Header
