import React from 'react'
import { Box, Button } from '@mui/material'
import { colors } from '../../theme/colors'

interface PaginationProps {
  callPrevious: () => void
  callNext: () => void
  disablePreviousButton: boolean
  disableNextButton: boolean
}

const Pagination: React.FC<PaginationProps> = ({ callPrevious, callNext, disablePreviousButton, disableNextButton }: PaginationProps) => {
  return (
    <Box my={4} style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
      <Button
        variant="contained"
        style={{ backgroundColor: colors.neutral30, height: '56px', width: '83px', fontFamily: 'Roboto Condensed', fontWeight: 700, fontSize: '18px', textTransform: 'none', marginRight: '6px' }}
        onClick={callPrevious}
        disabled={disablePreviousButton}
      >
          Prev
      </Button>
      <Button
        variant="contained"
        style={{ backgroundColor: colors.neutral30, height: '56px', width: '83px', fontFamily: 'Roboto Condensed', fontWeight: 700, fontSize: '18px', textTransform: 'none', marginLeft: '6px' }}
        onClick={callNext}
        disabled={disableNextButton}
      >
        Next
      </Button>
    </Box>
  )
}

export default Pagination
