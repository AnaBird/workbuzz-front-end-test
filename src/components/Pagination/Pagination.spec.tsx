import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Pagination from './Pagination'
import { vi } from 'vitest'

const callNext = vi.fn()
const callPrevious = vi.fn()

describe('Pagination', () => {
  it('should render the pagination buttons', () => {
    const { getByText } = render(<Pagination callNext={callNext} callPrevious={callPrevious} disableNextButton={false} disablePreviousButton={true} />)
    expect(getByText('Prev')).toBeInTheDocument()
    expect(getByText('Next')).toBeInTheDocument()
  })
})
