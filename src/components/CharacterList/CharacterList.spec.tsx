import React from 'react'
import { fireEvent, render } from '@testing-library/react'
import '@testing-library/jest-dom'
import CharacterList from './CharactersList'
import { DataContext } from '../../context/DataContext'
import { mockComicData } from '../../context/mockData'

const data = {
  comics: mockComicData,
  characters: [],
  fetchCharacters: vi.fn(),
  fetchComics: vi.fn()
}

const setup = (): any => {
  return render(
    <DataContext.Provider value={data}>
      <CharacterList />
    </DataContext.Provider>
  )
}

describe('CharacterList', () => {
  it('shows the comic list component', () => {
    const { getByText, getByTestId } = setup()
    expect(getByText('Comics')).toBeInTheDocument()
    expect(getByTestId('comicCard')).toBeInTheDocument()
  })
  it('should call th api when next or prev are clicked', () => {
    const { getByText } = setup()
    const nextBtn = getByText('Next')
    expect(nextBtn).toBeInTheDocument()
    fireEvent.click(nextBtn)
  })
})
