import React, { useEffect, useState } from 'react'
import { Box, Container, Grid, Typography } from '@mui/material'
import { useDataContext } from '../../context/useDataContext'
import Title from '../Title/Title'
import ReadingList from '../ReadingList/ReadingList'
import FilterBy from '../FilterBy/FilterBy'
import ComicCard from '../ComicCard/ComicCard'
import Pagination from '../Pagination/Pagination'

const CharacterList: React.FC = () => {
  const { fetchComics, fetchCharacters, comics, characters } = useDataContext()
  const [fetchedComics, setFetchedComics] = useState<boolean>(false)
  const [fetchedCharacters, setFetchedCharacters] = useState<boolean>(false)
  const [pageNumber, setPageNumber] = useState<number>(0)
  // This would be built upon to add comics to the reading list
  const [readingListCount] = useState<number>(0)

  const callNext = (): void => {
    const newPageNumber = pageNumber + 1
    setPageNumber(newPageNumber)
    const offset = newPageNumber * 4
    fetchComics(offset)
  }

  const callPrevious = (): void => {
    if (pageNumber > 0) {
      const newPageNumber = pageNumber - 1
      setPageNumber(newPageNumber)
      const offset = newPageNumber * 4
      fetchComics(offset)
    }
  }

  const getIsLastPage = (): boolean => {
    let isLastPage: boolean = false
    if (comics.count !== undefined && comics.offset !== undefined) {
      const numberOfReturnedComics = comics?.offset + comics?.count
      isLastPage = numberOfReturnedComics === comics.total
    }
    return isLastPage
  }

  useEffect(() => {
    if (comics?.results?.length === 0 && !fetchedComics) {
      fetchComics()
      setFetchedComics(true)
    }
  }, [comics, fetchedComics])

  useEffect(() => {
    if (characters?.length === 0 && !fetchedCharacters) {
      fetchCharacters()
      setFetchedCharacters(true)
    }
  }, [fetchedCharacters, characters])

  return (
    <Container>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Title title="Comics" />
        <ReadingList number={readingListCount} />
      </Box>
      <FilterBy filterString="Filter by character:" options={characters} />
      <Grid container marginTop={5}>
        {comics?.results?.map((comic, index) =>
          <Grid item xs={12} sm={6} key={index}>
            <ComicCard comic={comic} />
          </Grid>
        )}
      </Grid>
      <Box>
        <Pagination callNext={callNext} callPrevious={callPrevious} disablePreviousButton={pageNumber === 0} disableNextButton={getIsLastPage()} />
      </Box>
      <Box display='flex' justifyContent='center'>
        <Typography variant='body2'>
          Data provided by Marvel. © 2014 Marvel
        </Typography>
      </Box>
    </Container>
  )
}

export default CharacterList
