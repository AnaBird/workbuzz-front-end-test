import React, { useMemo } from 'react'
import { Box, Grid, Typography, Button } from '@mui/material'
import { type Comic } from '../../context/types'
import { colors } from '../../theme/colors'
import moment from 'moment'

interface ComicCardProps {
  comic: Comic
}

const ComicCard: React.FC<ComicCardProps> = ({ comic }: ComicCardProps) => {
  const writersArray = comic.creators.items.filter((creator) => creator.role === 'writer')

  const getWritersString = useMemo((): string => {
    let textString = ''
    writersArray.forEach((writer, index) => {
      textString = textString + `${index > 0 ? ' ' : ''}` + `${writer.name}${index < writersArray.length - 1 ? ',' : ''}`
    })
    return textString
  }, [writersArray])

  const getComicDate = useMemo((): string => {
    const publishedDate = comic.dates.find((date) => date.type === 'onsaleDate')
    return (publishedDate != null) ? moment(publishedDate.date).format('MMMM D, YYYY') : '-'
  }, [])

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6} data-testid='comicCard' marginBottom={5}>
        <Box style={{ position: 'relative' }}>
          <Box style={{ position: 'absolute', bottom: 20, left: 0, width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button variant="contained" style={{ backgroundColor: colors.primary, height: '56px', width: '83px', fontFamily: 'Roboto Condensed', fontWeight: 700, fontSize: '18px', textTransform: 'none' }}>Add</Button>
          </Box>
          <img src={`${comic.thumbnail.path}.${comic.thumbnail.extension}`} style={{ width: '100%', height: '100%', objectFit: 'cover' }}/>
        </Box>
      </Grid>
      <Grid item xs={12} sm={6} paddingRight={{ xs: 0, sm: 2 }}>
        <Box mb={4}>
          <Typography variant='h2'>
            {comic.title}
          </Typography>
        </Box>
        <Box my={2}>
          <Typography variant='h4'>
            Published:
          </Typography>
          <Typography variant='body1'>
            {getComicDate}
          </Typography>
        </Box>
        <Box my={2}>
          <Typography variant='h4'>
            Writer:
          </Typography>
          <Typography variant='body1'>
            {getWritersString}
          </Typography>
        </Box>
        <Box my={2}>
          <Typography variant='body2'>
            {comic.description}
          </Typography>
        </Box>
      </Grid>
    </Grid>
  )
}

export default ComicCard
