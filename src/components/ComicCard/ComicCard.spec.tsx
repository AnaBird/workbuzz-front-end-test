import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import ComicCard from './ComicCard'
import { mockComicData } from '../../context/mockData'

const mockComicData2 = {
  creators: {
    available: 3,
    collectionURI: '',
    items: [{
      name: 'Christos Gage',
      resourceURI: 'http://gateway.marvel.com/v1/public/creators/11765',
      role: 'writer'
    }, {
      name: 'Dan Slott',
      resourceURI: 'http://gateway.marvel.com/v1/public/creators/12983',
      role: 'writer'
    }, {
      name: 'Jay David Ramos',
      resourceURI: 'http://gateway.marvel.com/v1/public/creators/8706',
      role: 'colorist'
    }],
    returned: 3
  },
  dates: [{
    date: new Date('2008-09-24T00:00:00-0400'),
    type: 'releaseDate'
  }],
  description: 'Mock description',
  title: 'Avengers: The Initiative (2007) #17',
  thumbnail: {
    extension: 'jpg',
    path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/a0/58dd03dc2ec00'
  }
}

describe('ComicCard', () => {
  it('shows the comic data', () => {
    const { getByText } = render(<ComicCard comic={mockComicData.results[0]} />)
    expect(getByText('Avengers: The Initiative (2007) #17')).toBeInTheDocument()
    expect(getByText('Christos Gage, Dan Slott')).toBeInTheDocument()
    expect(getByText('September 24, 2008')).toBeInTheDocument()
    expect(getByText('Mock description')).toBeInTheDocument()
  })
  it('shows a - if no date found', () => {
    const { getByText } = render(<ComicCard comic={mockComicData2} />)
    expect(getByText('-')).toBeInTheDocument()
  })
})
