import React from 'react'
import { Box, Typography } from '@mui/material'

interface ReadingListProps {
  number: number
}

const ReadingList: React.FC<ReadingListProps> = ({ number }: ReadingListProps) => {
  return (
    <Box>
      <Typography color='white' fontSize='18px'>
        Reading List ({number})
      </Typography>
    </Box>
  )
}

export default ReadingList
