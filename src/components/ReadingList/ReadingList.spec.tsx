import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import ReadingList from './ReadingList'

describe('ReadingList', () => {
  it('shows the reading list total', () => {
    const { getByText } = render(<ReadingList number={2} />)
    expect(getByText('Reading List (2)')).toBeInTheDocument()
  })
})
