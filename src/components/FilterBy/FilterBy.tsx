import React from 'react'
import { Box, Button, Typography } from '@mui/material'
import { type Character } from '../../context/types'
import { colors } from '../../theme/colors'

interface FilterByProps {
  filterString: string
  options: Character[]
}

const FilterBy: React.FC<FilterByProps> = ({ filterString, options }: FilterByProps) => {
  return (
    <Box width="100%" style={{ backgroundColor: '#393939', borderRadius: '16px' }} padding="24px">
      <Typography color='#AEAAAE' fontSize='18px' fontWeight="700">
        {filterString}
      </Typography>
      {options.map((option, index) =>
        <Button variant="text" key={index} style={{ fontFamily: 'Roboto', fontWeight: 400, lineHeight: '27px', textTransform: 'none', color: colors.white, fontSize: '18px', paddingLeft: '0px', marginRight: '12px' }}>{option.name}</Button>
      )}
    </Box>
  )
}

export default FilterBy
