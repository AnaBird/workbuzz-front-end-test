import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import FilterBy from './FilterBy'

const mockCharacterData = [
  {
    name: '3D-Man',
    collectionUri: ''
  },
  {
    name: 'A-Bomb (HAS)',
    collectionUri: ''
  }
]

describe('FilterBy', () => {
  it('should show the Filters', () => {
    const { getByText } = render(<FilterBy filterString="Filter by character" options={mockCharacterData} />)
    expect(getByText('Filter by character')).toBeInTheDocument()
    expect(getByText('3D-Man')).toBeInTheDocument()
    expect(getByText('A-Bomb (HAS)')).toBeInTheDocument()
  })
})
