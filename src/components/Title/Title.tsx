import React from 'react'
import { Box, Typography } from '@mui/material'

interface HeaderTitleProps {
  title: string
}

const Title: React.FC<HeaderTitleProps> = ({ title }: HeaderTitleProps) => {
  // Need to add styling here

  return (
    <Box my={4}>
      <Typography variant='h1' color='white' fontSize='44px' fontWeight="700">
        {title}
      </Typography>
    </Box>
  )
}

export default Title
