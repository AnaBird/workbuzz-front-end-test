import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom'
import Title from './Title'

describe('Title', () => {
  it('shows the reading title', () => {
    const { getByText } = render(<Title title='Comics' />)
    expect(getByText('Comics')).toBeInTheDocument()
  })
})
